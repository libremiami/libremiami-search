# LibreMiami Search

Our fork of searx

A privacy-respecting, hackable [metasearch engine](https://en.wikipedia.org/wiki/Metasearch_engine)

[See it in action](search.libremiami.org)

# Dev

## Dependencies

You need virtualenv and less with the clean-css plugin

```
pip install virtualenv
```
```
npm install less less-plugin-clean-css
```

Then the makefile works 
```
make themes
make run
```